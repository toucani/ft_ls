# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dkovalch <dkovalch@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/29 12:53:07 by dkovalch          #+#    #+#              #
#    Updated: 2017/10/29 12:53:08 by dkovalch         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls
CC = gcc
CFLAGS = -O3 -Wall -Werror -Wextra
IFLAGS = -I includes -I $(LIBFT) -I $(FT_PRINTF)

LIBFT = libraries/libft
FT_PRINTF = libraries/ft_printf

SOURCES_FLD_1 = sources
OBJECTS_FLD = objects
HEADERS_FLD_1 = includes

OBJECTS_1 =\
	$(OBJECTS_FLD)/ft_ls_list.o\
	$(OBJECTS_FLD)/ft_ls_main.o\
	$(OBJECTS_FLD)/ft_main.o\
	$(OBJECTS_FLD)/ft_ls_print.o\
	$(OBJECTS_FLD)/ft_ls_compute.o\
	$(OBJECTS_FLD)/ft_ls_help.o\
	$(OBJECTS_FLD)/ft_ls_sort.o


all : $(NAME)

$(NAME) : $(LIBFT)/libft.a $(FT_PRINTF)/libftprintf.a $(OBJECTS_1)
	$(CC) $(IFLAGS) $(CFLAGS) $(OBJECTS_1) -L $(LIBFT) -lft -L $(FT_PRINTF) -lftprintf -o $(NAME)

$(LIBFT)/libft.a :
	@echo "\t\tMaking libft in $(LIBFT)"
	@make -C $(LIBFT)
	@echo "\t\tDone making libft\n"

$(FT_PRINTF)/libftprintf.a :
	@echo "\t\tMaking ft_printf in $(FT_PRINTF)"
	@make -C $(FT_PRINTF)
	@echo "\t\tDone making ft_printf\n"

debug :
	make re -C . CFLAGS="$(CFLAGS) -Og -g"


clean :
	make clean -C $(LIBFT)
	make clean -C $(FT_PRINTF)
	rm -f $(OBJECTS_1)

fclean : clean
	make fclean -C $(LIBFT)
	make fclean -C $(FT_PRINTF)
	rm -f $(NAME)

re : fclean all

dre : dclean debug

$(OBJECTS_FLD)/ft_ls_list.o : $(SOURCES_FLD_1)/ft_ls_list.c
	@mkdir -p $(OBJECTS_FLD)
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_ls_list.c -o $(OBJECTS_FLD)/ft_ls_list.o

$(OBJECTS_FLD)/ft_ls_main.o : $(SOURCES_FLD_1)/ft_ls_main.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_ls_main.c -o $(OBJECTS_FLD)/ft_ls_main.o

$(OBJECTS_FLD)/ft_ls_help.o : $(SOURCES_FLD_1)/ft_ls_help.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_ls_help.c -o $(OBJECTS_FLD)/ft_ls_help.o

$(OBJECTS_FLD)/ft_ls_sort.o : $(SOURCES_FLD_1)/ft_ls_sort.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_ls_sort.c -o $(OBJECTS_FLD)/ft_ls_sort.o

$(OBJECTS_FLD)/ft_main.o : $(SOURCES_FLD_1)/ft_main.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_main.c -o $(OBJECTS_FLD)/ft_main.o

$(OBJECTS_FLD)/ft_ls_print.o : $(SOURCES_FLD_1)/ft_ls_print.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_ls_print.c -o $(OBJECTS_FLD)/ft_ls_print.o

$(OBJECTS_FLD)/ft_ls_compute.o : $(SOURCES_FLD_1)/ft_ls_compute.c
	$(CC) $(IFLAGS) $(CFLAGS) -c $(SOURCES_FLD_1)/ft_ls_compute.c -o $(OBJECTS_FLD)/ft_ls_compute.o

# DO NOT DELETE

sources/ft_ls_compute.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_ls_compute.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_ls_compute.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_ls_compute.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_ls_compute.o: /usr/include/sys/_posix_availability.h
sources/ft_ls_compute.o: /usr/include/machine/_types.h
sources/ft_ls_compute.o: /usr/include/i386/_types.h
sources/ft_ls_compute.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_ls_compute.o: /usr/include/sys/unistd.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_seek_set.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_size_t.h
sources/ft_ls_compute.o: /usr/include/_types/_uint64_t.h
sources/ft_ls_compute.o: /usr/include/Availability.h
sources/ft_ls_compute.o: /usr/include/AvailabilityInternal.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_uid_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_gid_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_off_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_pid_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_null.h
sources/ft_ls_compute.o: /usr/include/sys/select.h
sources/ft_ls_compute.o: /usr/include/sys/appleapiopts.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_def.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_timespec.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_timeval.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_time_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_set.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_ls_compute.o: /usr/include/sys/_select.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_dev_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_mode_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_ls_compute.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_offsetof.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_wint_t.h
sources/ft_ls_compute.o: /usr/include/stdlib.h /usr/include/sys/wait.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_id_t.h
sources/ft_ls_compute.o: /usr/include/sys/signal.h
sources/ft_ls_compute.o: /usr/include/machine/signal.h
sources/ft_ls_compute.o: /usr/include/i386/signal.h
sources/ft_ls_compute.o: /usr/include/machine/_mcontext.h
sources/ft_ls_compute.o: /usr/include/i386/_mcontext.h
sources/ft_ls_compute.o: /usr/include/mach/i386/_structs.h
sources/ft_ls_compute.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_ucontext.h
sources/ft_ls_compute.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_int8_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_int16_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_int32_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_int64_t.h
sources/ft_ls_compute.o: /usr/include/_types/_uint8_t.h
sources/ft_ls_compute.o: /usr/include/_types/_uint16_t.h
sources/ft_ls_compute.o: /usr/include/_types/_uint32_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_ls_compute.o: /usr/include/_types/_intmax_t.h
sources/ft_ls_compute.o: /usr/include/_types/_uintmax_t.h
sources/ft_ls_compute.o: /usr/include/machine/endian.h
sources/ft_ls_compute.o: /usr/include/i386/endian.h
sources/ft_ls_compute.o: /usr/include/sys/_endian.h
sources/ft_ls_compute.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_ls_compute.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_ls_compute.o: /usr/include/alloca.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_rune_t.h
sources/ft_ls_compute.o: /usr/include/machine/types.h
sources/ft_ls_compute.o: /usr/include/i386/types.h /usr/include/inttypes.h
sources/ft_ls_compute.o: libraries/libft/get_next_line.h /usr/include/stdio.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_va_list.h
sources/ft_ls_compute.o: /usr/include/sys/stdio.h
sources/ft_ls_compute.o: /usr/include/secure/_stdio.h
sources/ft_ls_compute.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_ls_compute.o: /usr/include/machine/limits.h
sources/ft_ls_compute.o: /usr/include/i386/limits.h
sources/ft_ls_compute.o: /usr/include/i386/_limits.h
sources/ft_ls_compute.o: /usr/include/sys/syslimits.h /usr/include/sys/stat.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_ino_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_ls_compute.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_ls_compute.o: /usr/include/pwd.h /usr/include/uuid/uuid.h
sources/ft_ls_compute.o: /usr/include/grp.h
sources/ft_ls_help.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_ls_help.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_ls_help.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_ls_help.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_ls_help.o: /usr/include/sys/_posix_availability.h
sources/ft_ls_help.o: /usr/include/machine/_types.h
sources/ft_ls_help.o: /usr/include/i386/_types.h
sources/ft_ls_help.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_ls_help.o: /usr/include/sys/unistd.h
sources/ft_ls_help.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_ls_help.o: /usr/include/sys/_types/_seek_set.h
sources/ft_ls_help.o: /usr/include/sys/_types/_size_t.h
sources/ft_ls_help.o: /usr/include/_types/_uint64_t.h
sources/ft_ls_help.o: /usr/include/Availability.h
sources/ft_ls_help.o: /usr/include/AvailabilityInternal.h
sources/ft_ls_help.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_uid_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_gid_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_off_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_pid_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_null.h
sources/ft_ls_help.o: /usr/include/sys/select.h
sources/ft_ls_help.o: /usr/include/sys/appleapiopts.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_def.h
sources/ft_ls_help.o: /usr/include/sys/_types/_timespec.h
sources/ft_ls_help.o: /usr/include/sys/_types/_timeval.h
sources/ft_ls_help.o: /usr/include/sys/_types/_time_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_set.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_ls_help.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_ls_help.o: /usr/include/sys/_select.h
sources/ft_ls_help.o: /usr/include/sys/_types/_dev_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_mode_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_ls_help.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_ls_help.o: /usr/include/sys/_types/_offsetof.h
sources/ft_ls_help.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_ls_help.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_ls_help.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_ls_help.o: /usr/include/i386/signal.h
sources/ft_ls_help.o: /usr/include/machine/_mcontext.h
sources/ft_ls_help.o: /usr/include/i386/_mcontext.h
sources/ft_ls_help.o: /usr/include/mach/i386/_structs.h
sources/ft_ls_help.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_ls_help.o: /usr/include/sys/_types/_ucontext.h
sources/ft_ls_help.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_ls_help.o: /usr/include/sys/_types/_int8_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_int16_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_int32_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_int64_t.h
sources/ft_ls_help.o: /usr/include/_types/_uint8_t.h
sources/ft_ls_help.o: /usr/include/_types/_uint16_t.h
sources/ft_ls_help.o: /usr/include/_types/_uint32_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_ls_help.o: /usr/include/_types/_intmax_t.h
sources/ft_ls_help.o: /usr/include/_types/_uintmax_t.h
sources/ft_ls_help.o: /usr/include/machine/endian.h
sources/ft_ls_help.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_ls_help.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_ls_help.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_ls_help.o: /usr/include/alloca.h
sources/ft_ls_help.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_rune_t.h
sources/ft_ls_help.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_ls_help.o: /usr/include/inttypes.h libraries/libft/get_next_line.h
sources/ft_ls_help.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_ls_help.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_ls_help.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_ls_help.o: /usr/include/machine/limits.h
sources/ft_ls_help.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_ls_help.o: /usr/include/sys/syslimits.h /usr/include/sys/stat.h
sources/ft_ls_help.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_ino_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_ls_help.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_ls_help.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_ls_help.o: libraries/ft_printf/ft_printf.h
sources/ft_ls_list.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_ls_list.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_ls_list.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_ls_list.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_ls_list.o: /usr/include/sys/_posix_availability.h
sources/ft_ls_list.o: /usr/include/machine/_types.h
sources/ft_ls_list.o: /usr/include/i386/_types.h
sources/ft_ls_list.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_ls_list.o: /usr/include/sys/unistd.h
sources/ft_ls_list.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_ls_list.o: /usr/include/sys/_types/_seek_set.h
sources/ft_ls_list.o: /usr/include/sys/_types/_size_t.h
sources/ft_ls_list.o: /usr/include/_types/_uint64_t.h
sources/ft_ls_list.o: /usr/include/Availability.h
sources/ft_ls_list.o: /usr/include/AvailabilityInternal.h
sources/ft_ls_list.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_uid_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_gid_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_off_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_pid_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_null.h
sources/ft_ls_list.o: /usr/include/sys/select.h
sources/ft_ls_list.o: /usr/include/sys/appleapiopts.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_def.h
sources/ft_ls_list.o: /usr/include/sys/_types/_timespec.h
sources/ft_ls_list.o: /usr/include/sys/_types/_timeval.h
sources/ft_ls_list.o: /usr/include/sys/_types/_time_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_set.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_ls_list.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_ls_list.o: /usr/include/sys/_select.h
sources/ft_ls_list.o: /usr/include/sys/_types/_dev_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_mode_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_ls_list.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_ls_list.o: /usr/include/sys/_types/_offsetof.h
sources/ft_ls_list.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_ls_list.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_ls_list.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_ls_list.o: /usr/include/i386/signal.h
sources/ft_ls_list.o: /usr/include/machine/_mcontext.h
sources/ft_ls_list.o: /usr/include/i386/_mcontext.h
sources/ft_ls_list.o: /usr/include/mach/i386/_structs.h
sources/ft_ls_list.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_ls_list.o: /usr/include/sys/_types/_ucontext.h
sources/ft_ls_list.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_ls_list.o: /usr/include/sys/_types/_int8_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_int16_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_int32_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_int64_t.h
sources/ft_ls_list.o: /usr/include/_types/_uint8_t.h
sources/ft_ls_list.o: /usr/include/_types/_uint16_t.h
sources/ft_ls_list.o: /usr/include/_types/_uint32_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_ls_list.o: /usr/include/_types/_intmax_t.h
sources/ft_ls_list.o: /usr/include/_types/_uintmax_t.h
sources/ft_ls_list.o: /usr/include/machine/endian.h
sources/ft_ls_list.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_ls_list.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_ls_list.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_ls_list.o: /usr/include/alloca.h
sources/ft_ls_list.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_rune_t.h
sources/ft_ls_list.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_ls_list.o: /usr/include/inttypes.h libraries/libft/get_next_line.h
sources/ft_ls_list.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_ls_list.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_ls_list.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_ls_list.o: /usr/include/machine/limits.h
sources/ft_ls_list.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_ls_list.o: /usr/include/sys/syslimits.h /usr/include/sys/stat.h
sources/ft_ls_list.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_ino_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_ls_list.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_ls_list.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_ls_main.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_ls_main.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_ls_main.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_ls_main.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_ls_main.o: /usr/include/sys/_posix_availability.h
sources/ft_ls_main.o: /usr/include/machine/_types.h
sources/ft_ls_main.o: /usr/include/i386/_types.h
sources/ft_ls_main.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_ls_main.o: /usr/include/sys/unistd.h
sources/ft_ls_main.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_ls_main.o: /usr/include/sys/_types/_seek_set.h
sources/ft_ls_main.o: /usr/include/sys/_types/_size_t.h
sources/ft_ls_main.o: /usr/include/_types/_uint64_t.h
sources/ft_ls_main.o: /usr/include/Availability.h
sources/ft_ls_main.o: /usr/include/AvailabilityInternal.h
sources/ft_ls_main.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_uid_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_gid_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_off_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_pid_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_null.h
sources/ft_ls_main.o: /usr/include/sys/select.h
sources/ft_ls_main.o: /usr/include/sys/appleapiopts.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_def.h
sources/ft_ls_main.o: /usr/include/sys/_types/_timespec.h
sources/ft_ls_main.o: /usr/include/sys/_types/_timeval.h
sources/ft_ls_main.o: /usr/include/sys/_types/_time_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_set.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_ls_main.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_ls_main.o: /usr/include/sys/_select.h
sources/ft_ls_main.o: /usr/include/sys/_types/_dev_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_mode_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_ls_main.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_ls_main.o: /usr/include/sys/_types/_offsetof.h
sources/ft_ls_main.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_ls_main.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_ls_main.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_ls_main.o: /usr/include/i386/signal.h
sources/ft_ls_main.o: /usr/include/machine/_mcontext.h
sources/ft_ls_main.o: /usr/include/i386/_mcontext.h
sources/ft_ls_main.o: /usr/include/mach/i386/_structs.h
sources/ft_ls_main.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_ls_main.o: /usr/include/sys/_types/_ucontext.h
sources/ft_ls_main.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_ls_main.o: /usr/include/sys/_types/_int8_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_int16_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_int32_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_int64_t.h
sources/ft_ls_main.o: /usr/include/_types/_uint8_t.h
sources/ft_ls_main.o: /usr/include/_types/_uint16_t.h
sources/ft_ls_main.o: /usr/include/_types/_uint32_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_ls_main.o: /usr/include/_types/_intmax_t.h
sources/ft_ls_main.o: /usr/include/_types/_uintmax_t.h
sources/ft_ls_main.o: /usr/include/machine/endian.h
sources/ft_ls_main.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_ls_main.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_ls_main.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_ls_main.o: /usr/include/alloca.h
sources/ft_ls_main.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_rune_t.h
sources/ft_ls_main.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_ls_main.o: /usr/include/inttypes.h libraries/libft/get_next_line.h
sources/ft_ls_main.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_ls_main.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_ls_main.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_ls_main.o: /usr/include/machine/limits.h
sources/ft_ls_main.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_ls_main.o: /usr/include/sys/syslimits.h /usr/include/sys/stat.h
sources/ft_ls_main.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_ino_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_ls_main.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_ls_main.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_ls_main.o: libraries/ft_printf/ft_printf.h /usr/include/errno.h
sources/ft_ls_main.o: /usr/include/sys/errno.h
sources/ft_ls_main.o: /usr/include/sys/_types/_errno_t.h
sources/ft_ls_main.o: /usr/include/dirent.h /usr/include/sys/dirent.h
sources/ft_ls_print.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_ls_print.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_ls_print.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_ls_print.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_ls_print.o: /usr/include/sys/_posix_availability.h
sources/ft_ls_print.o: /usr/include/machine/_types.h
sources/ft_ls_print.o: /usr/include/i386/_types.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_ls_print.o: /usr/include/sys/unistd.h
sources/ft_ls_print.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_ls_print.o: /usr/include/sys/_types/_seek_set.h
sources/ft_ls_print.o: /usr/include/sys/_types/_size_t.h
sources/ft_ls_print.o: /usr/include/_types/_uint64_t.h
sources/ft_ls_print.o: /usr/include/Availability.h
sources/ft_ls_print.o: /usr/include/AvailabilityInternal.h
sources/ft_ls_print.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_uid_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_gid_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_off_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_pid_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_null.h
sources/ft_ls_print.o: /usr/include/sys/select.h
sources/ft_ls_print.o: /usr/include/sys/appleapiopts.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_def.h
sources/ft_ls_print.o: /usr/include/sys/_types/_timespec.h
sources/ft_ls_print.o: /usr/include/sys/_types/_timeval.h
sources/ft_ls_print.o: /usr/include/sys/_types/_time_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_set.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_ls_print.o: /usr/include/sys/_select.h
sources/ft_ls_print.o: /usr/include/sys/_types/_dev_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_mode_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_ls_print.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_ls_print.o: /usr/include/sys/_types/_offsetof.h
sources/ft_ls_print.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_wint_t.h
sources/ft_ls_print.o: /usr/include/stdlib.h /usr/include/sys/wait.h
sources/ft_ls_print.o: /usr/include/sys/_types/_id_t.h
sources/ft_ls_print.o: /usr/include/sys/signal.h
sources/ft_ls_print.o: /usr/include/machine/signal.h
sources/ft_ls_print.o: /usr/include/i386/signal.h
sources/ft_ls_print.o: /usr/include/machine/_mcontext.h
sources/ft_ls_print.o: /usr/include/i386/_mcontext.h
sources/ft_ls_print.o: /usr/include/mach/i386/_structs.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_ls_print.o: /usr/include/sys/_types/_ucontext.h
sources/ft_ls_print.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_ls_print.o: /usr/include/sys/_types/_int8_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_int16_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_int32_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_int64_t.h
sources/ft_ls_print.o: /usr/include/_types/_uint8_t.h
sources/ft_ls_print.o: /usr/include/_types/_uint16_t.h
sources/ft_ls_print.o: /usr/include/_types/_uint32_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_ls_print.o: /usr/include/_types/_intmax_t.h
sources/ft_ls_print.o: /usr/include/_types/_uintmax_t.h
sources/ft_ls_print.o: /usr/include/machine/endian.h
sources/ft_ls_print.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_ls_print.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_ls_print.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_ls_print.o: /usr/include/alloca.h
sources/ft_ls_print.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_rune_t.h
sources/ft_ls_print.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_ls_print.o: /usr/include/inttypes.h
sources/ft_ls_print.o: libraries/libft/get_next_line.h /usr/include/stdio.h
sources/ft_ls_print.o: /usr/include/sys/_types/_va_list.h
sources/ft_ls_print.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_ls_print.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_ls_print.o: /usr/include/machine/limits.h
sources/ft_ls_print.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_ls_print.o: /usr/include/sys/syslimits.h /usr/include/sys/stat.h
sources/ft_ls_print.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_ino_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_ls_print.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_ls_print.o: /usr/include/time.h /usr/include/sys/_types/_clock_t.h
sources/ft_ls_print.o: /usr/include/pwd.h /usr/include/uuid/uuid.h
sources/ft_ls_print.o: /usr/include/grp.h libraries/ft_printf/ft_printf.h
sources/ft_ls_print.o: /usr/include/sys/ioctl.h /usr/include/sys/ttycom.h
sources/ft_ls_print.o: /usr/include/sys/ioccom.h /usr/include/sys/filio.h
sources/ft_ls_print.o: /usr/include/sys/sockio.h /usr/include/sys/xattr.h
sources/ft_ls_print.o: /usr/include/sys/types.h
sources/ft_ls_print.o: /usr/include/sys/_types/_in_addr_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_in_port_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_key_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_errno_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_cond_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_condattr_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_mutex_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_mutexattr_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_once_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_rwlock_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_rwlockattr_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_t.h
sources/ft_ls_print.o: /usr/include/sys/_pthread/_pthread_key_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fsblkcnt_t.h
sources/ft_ls_print.o: /usr/include/sys/_types/_fsfilcnt_t.h
sources/ft_ls_sort.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_ls_sort.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_ls_sort.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_ls_sort.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_ls_sort.o: /usr/include/sys/_posix_availability.h
sources/ft_ls_sort.o: /usr/include/machine/_types.h
sources/ft_ls_sort.o: /usr/include/i386/_types.h
sources/ft_ls_sort.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_ls_sort.o: /usr/include/sys/unistd.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_seek_set.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_size_t.h
sources/ft_ls_sort.o: /usr/include/_types/_uint64_t.h
sources/ft_ls_sort.o: /usr/include/Availability.h
sources/ft_ls_sort.o: /usr/include/AvailabilityInternal.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_uid_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_gid_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_off_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_pid_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_null.h
sources/ft_ls_sort.o: /usr/include/sys/select.h
sources/ft_ls_sort.o: /usr/include/sys/appleapiopts.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_def.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_timespec.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_timeval.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_time_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_set.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_ls_sort.o: /usr/include/sys/_select.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_dev_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_mode_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_ls_sort.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_offsetof.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_ls_sort.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_ls_sort.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_ls_sort.o: /usr/include/i386/signal.h
sources/ft_ls_sort.o: /usr/include/machine/_mcontext.h
sources/ft_ls_sort.o: /usr/include/i386/_mcontext.h
sources/ft_ls_sort.o: /usr/include/mach/i386/_structs.h
sources/ft_ls_sort.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_ucontext.h
sources/ft_ls_sort.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_int8_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_int16_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_int32_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_int64_t.h
sources/ft_ls_sort.o: /usr/include/_types/_uint8_t.h
sources/ft_ls_sort.o: /usr/include/_types/_uint16_t.h
sources/ft_ls_sort.o: /usr/include/_types/_uint32_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_ls_sort.o: /usr/include/_types/_intmax_t.h
sources/ft_ls_sort.o: /usr/include/_types/_uintmax_t.h
sources/ft_ls_sort.o: /usr/include/machine/endian.h
sources/ft_ls_sort.o: /usr/include/i386/endian.h /usr/include/sys/_endian.h
sources/ft_ls_sort.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_ls_sort.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_ls_sort.o: /usr/include/alloca.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_ct_rune_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_rune_t.h
sources/ft_ls_sort.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_ls_sort.o: /usr/include/inttypes.h libraries/libft/get_next_line.h
sources/ft_ls_sort.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_ls_sort.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_ls_sort.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_ls_sort.o: /usr/include/machine/limits.h
sources/ft_ls_sort.o: /usr/include/i386/limits.h /usr/include/i386/_limits.h
sources/ft_ls_sort.o: /usr/include/sys/syslimits.h /usr/include/sys/stat.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_ino_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_ls_sort.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_main.o: includes/ft_ls.h libraries/libft/libft.h
sources/ft_main.o: /usr/include/unistd.h /usr/include/_types.h
sources/ft_main.o: /usr/include/sys/_types.h /usr/include/sys/cdefs.h
sources/ft_main.o: /usr/include/sys/_symbol_aliasing.h
sources/ft_main.o: /usr/include/sys/_posix_availability.h
sources/ft_main.o: /usr/include/machine/_types.h /usr/include/i386/_types.h
sources/ft_main.o: /usr/include/sys/_pthread/_pthread_types.h
sources/ft_main.o: /usr/include/sys/unistd.h
sources/ft_main.o: /usr/include/sys/_types/_posix_vdisable.h
sources/ft_main.o: /usr/include/sys/_types/_seek_set.h
sources/ft_main.o: /usr/include/sys/_types/_size_t.h
sources/ft_main.o: /usr/include/_types/_uint64_t.h
sources/ft_main.o: /usr/include/Availability.h
sources/ft_main.o: /usr/include/AvailabilityInternal.h
sources/ft_main.o: /usr/include/sys/_types/_ssize_t.h
sources/ft_main.o: /usr/include/sys/_types/_uid_t.h
sources/ft_main.o: /usr/include/sys/_types/_gid_t.h
sources/ft_main.o: /usr/include/sys/_types/_intptr_t.h
sources/ft_main.o: /usr/include/sys/_types/_off_t.h
sources/ft_main.o: /usr/include/sys/_types/_pid_t.h
sources/ft_main.o: /usr/include/sys/_types/_useconds_t.h
sources/ft_main.o: /usr/include/sys/_types/_null.h /usr/include/sys/select.h
sources/ft_main.o: /usr/include/sys/appleapiopts.h
sources/ft_main.o: /usr/include/sys/_types/_fd_def.h
sources/ft_main.o: /usr/include/sys/_types/_timespec.h
sources/ft_main.o: /usr/include/sys/_types/_timeval.h
sources/ft_main.o: /usr/include/sys/_types/_time_t.h
sources/ft_main.o: /usr/include/sys/_types/_suseconds_t.h
sources/ft_main.o: /usr/include/sys/_types/_sigset_t.h
sources/ft_main.o: /usr/include/sys/_types/_fd_setsize.h
sources/ft_main.o: /usr/include/sys/_types/_fd_set.h
sources/ft_main.o: /usr/include/sys/_types/_fd_clr.h
sources/ft_main.o: /usr/include/sys/_types/_fd_isset.h
sources/ft_main.o: /usr/include/sys/_types/_fd_zero.h
sources/ft_main.o: /usr/include/sys/_types/_fd_copy.h
sources/ft_main.o: /usr/include/sys/_select.h
sources/ft_main.o: /usr/include/sys/_types/_dev_t.h
sources/ft_main.o: /usr/include/sys/_types/_mode_t.h
sources/ft_main.o: /usr/include/sys/_types/_uuid_t.h
sources/ft_main.o: /usr/include/gethostuuid.h /usr/include/stddef.h
sources/ft_main.o: /usr/include/sys/_types/_offsetof.h
sources/ft_main.o: /usr/include/sys/_types/_ptrdiff_t.h
sources/ft_main.o: /usr/include/sys/_types/_rsize_t.h
sources/ft_main.o: /usr/include/sys/_types/_wchar_t.h
sources/ft_main.o: /usr/include/sys/_types/_wint_t.h /usr/include/stdlib.h
sources/ft_main.o: /usr/include/sys/wait.h /usr/include/sys/_types/_id_t.h
sources/ft_main.o: /usr/include/sys/signal.h /usr/include/machine/signal.h
sources/ft_main.o: /usr/include/i386/signal.h
sources/ft_main.o: /usr/include/machine/_mcontext.h
sources/ft_main.o: /usr/include/i386/_mcontext.h
sources/ft_main.o: /usr/include/mach/i386/_structs.h
sources/ft_main.o: /usr/include/sys/_pthread/_pthread_attr_t.h
sources/ft_main.o: /usr/include/sys/_types/_sigaltstack.h
sources/ft_main.o: /usr/include/sys/_types/_ucontext.h
sources/ft_main.o: /usr/include/sys/resource.h /usr/include/stdint.h
sources/ft_main.o: /usr/include/sys/_types/_int8_t.h
sources/ft_main.o: /usr/include/sys/_types/_int16_t.h
sources/ft_main.o: /usr/include/sys/_types/_int32_t.h
sources/ft_main.o: /usr/include/sys/_types/_int64_t.h
sources/ft_main.o: /usr/include/_types/_uint8_t.h
sources/ft_main.o: /usr/include/_types/_uint16_t.h
sources/ft_main.o: /usr/include/_types/_uint32_t.h
sources/ft_main.o: /usr/include/sys/_types/_uintptr_t.h
sources/ft_main.o: /usr/include/_types/_intmax_t.h
sources/ft_main.o: /usr/include/_types/_uintmax_t.h
sources/ft_main.o: /usr/include/machine/endian.h /usr/include/i386/endian.h
sources/ft_main.o: /usr/include/sys/_endian.h
sources/ft_main.o: /usr/include/libkern/_OSByteOrder.h
sources/ft_main.o: /usr/include/libkern/i386/_OSByteOrder.h
sources/ft_main.o: /usr/include/alloca.h /usr/include/sys/_types/_ct_rune_t.h
sources/ft_main.o: /usr/include/sys/_types/_rune_t.h
sources/ft_main.o: /usr/include/machine/types.h /usr/include/i386/types.h
sources/ft_main.o: /usr/include/inttypes.h libraries/libft/get_next_line.h
sources/ft_main.o: /usr/include/stdio.h /usr/include/sys/_types/_va_list.h
sources/ft_main.o: /usr/include/sys/stdio.h /usr/include/secure/_stdio.h
sources/ft_main.o: /usr/include/secure/_common.h /usr/include/limits.h
sources/ft_main.o: /usr/include/machine/limits.h /usr/include/i386/limits.h
sources/ft_main.o: /usr/include/i386/_limits.h /usr/include/sys/syslimits.h
sources/ft_main.o: /usr/include/sys/stat.h
sources/ft_main.o: /usr/include/sys/_types/_blkcnt_t.h
sources/ft_main.o: /usr/include/sys/_types/_blksize_t.h
sources/ft_main.o: /usr/include/sys/_types/_ino_t.h
sources/ft_main.o: /usr/include/sys/_types/_ino64_t.h
sources/ft_main.o: /usr/include/sys/_types/_nlink_t.h
sources/ft_main.o: /usr/include/sys/_types/_s_ifmt.h
sources/ft_main.o: /usr/include/sys/_types/_filesec_t.h
sources/ft_main.o: libraries/ft_printf/ft_printf.h /usr/include/dirent.h
sources/ft_main.o: /usr/include/sys/dirent.h /usr/include/errno.h
sources/ft_main.o: /usr/include/sys/errno.h
sources/ft_main.o: /usr/include/sys/_types/_errno_t.h

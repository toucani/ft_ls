/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 16:29:41 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/12 16:29:42 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FT_LS_H
# define FT_LS_H

# include "libft.h"
# include <limits.h>
# include <sys/stat.h>

/*
**	Arguments meaning:
**		1 - output in one column
**		a - show files starting with '.'
**		f - not sorting the output
**		G - colorful output
**		l - long format of output
**		R - recursive
**		r - reverse sorting order
**		S - sort by file size
**		t - sort by last modification time
**		u - sort by last access time (only with 't')
**
**		learn more by running "./ft_ls --help"
*/

# define ALL_ARGS		"1afGlRrStu"
# define ARG_ONE_COL	'1'
# define ARG_SHOW_DOT	'a'
# define ARG_SORT_NO	'f'
# define ARG_COLOR		'G'
# define ARG_LONG		'l'
# define ARG_RECURSIVE	'R'
# define ARG_SORT_REV	'r'
# define ARG_SORT_SIZE	'S'
# define ARG_SORT_T_MOD	't'
# define ARG_SORT_T_ACC	'u'

/*
**	Printing colors
*/

# define COLOR_FLD "bold cyan"
# define COLOR_EXE "bold red"
# define COLOR_LNK "bold green"
# define COLOR_FIF "bold blue"
# define COLOR_DEF "reset"

/*
**	Time after which we dont print time, but the year in long output
*/

# define SECS_IN_HALF_YEAR 15724800

/*
**	Maybe the maximum lengt of the filename
*/

# ifdef PATH_MAX
#  define MAX_FILENAME_LENGTH PATH_MAX
# else
#  define MAX_FILENAME_LENGTH 512
# endif

/*
**	rwx defines
**	User:	R W X and set-id
**	Group:	R W X and set-id
**	Other:	R W X and sticky bit
**	Is executable by any
*/

# define S_ISRUSR(m) ((m) & S_IRUSR)
# define S_ISWUSR(m) ((m) & S_IWUSR)
# define S_ISXUSR(m) ((m) & S_IXUSR)
# define S_ISSUID(m) ((m) & S_ISUID)

# define S_ISRGRP(m) ((m) & S_IRGRP)
# define S_ISWGRP(m) ((m) & S_IWGRP)
# define S_ISXGRP(m) ((m) & S_IXGRP)
# define S_ISSGID(m) ((m) & S_ISGID)

# define S_ISROTH(m) ((m) & S_IROTH)
# define S_ISWOTH(m) ((m) & S_IWOTH)
# define S_ISXOTH(m) ((m) & S_IXOTH)
# define S_ISSTCK(m) ((m) & S_ISVTX)

# define S_ISEXE(m) (((m) & S_IXUSR) || ((m) & S_IXGRP) || ((m) & S_IXOTH))

/*
**	Structure holding the array of paths to the same kind of files,
**	and counter(just for safety).
**	It is guaranteed that the paths array ends with NULL pointer.
*/

struct					s_paths
{
	char				**paths;
	unsigned int		count;
};

/*
**	Special structure, which holds paths to different kinds of files,
**	the user wants to see
*/

struct					s_ls_files
{
	struct s_paths		check;
	struct s_paths		dirs;
	struct s_paths		files;
};

/*
**	Holds the array of fileds to print with -l,
**	and name of the file, and it's color(if -G).
**
**	field_width[0]	-	width of the links	section
**	field_width[1]	-	width of the uid	section
**	field_width[2]	-	width of the gid	section
**	field_width[3]	-	width of the size	section
*/

struct					s_print_data
{
	char				*name;
	char				*name_color;
	uint16_t			field_width[4];
	uint16_t			name_width;
};

/*
**	Structure which holds the record for either a file or a folder.
**	_name_			-	should not be null(but it is possible)
**	_full_path_		-	can be null
**	_long_info_		-	can be null
**	_print_color_	-	can be null
**	_stats_			-	can not be null
**		(but it is possible, if the allocation fails)
**	_print_width_	-	can be 0
*/

typedef struct			s_ls_list
{
	char				*full_path;
	struct stat			stats;
	struct s_print_data	print;
	struct s_ls_list	*next;
}						t_ls_list;

/*
**	Main structure which holds arguments to the ls command, and colors
**	to print diferent kinds of files.
**
**	_args_			-	can be null
**	_program_name_	-	can NOT be null
**	_color_*_		-	can NOT be null
**	_color_def_		-	can be null
**	_sorting_		-	can be null
**	_reverse_		-	can be either 1 or -1, mustn't be 0!
*/

typedef struct			s_ls_args
{
	const char			*args;
	const char			*program_name;
	const char			*color_fld;
	const char			*color_exe;
	const char			*color_lnk;
	const char			*color_fifo;
	const char			*color_def;
	int					(*sorting)
		(const t_ls_list *restrict param1, const t_ls_list *restrict param2);
	int8_t				reverse;
}						t_ls_args;

/*
**	Sorting functions
*/

int						ft_sort_default(const t_ls_list *restrict param1,
										const t_ls_list *restrict param2);
int						ft_sort_time_access(const t_ls_list *restrict param1,
											const t_ls_list *restrict param2);
int						ft_sort_time_modify(const t_ls_list *restrict param1,
											const t_ls_list *restrict param2);
int						ft_sort_size(const t_ls_list *restrict param1,
									const t_ls_list *restrict param2);
int						ft_sort_no(const t_ls_list *restrict param1,
									const t_ls_list *restrict param2);

/*
**	Working functions
*/

t_ls_list				*ft_list_create_elem
				(const char *name, const char *full_path);
void					ft_list_push_back
				(t_ls_list **the_list, t_ls_list *restrict elem,
				const t_ls_args *restrict ls_args);
void					ft_ls_get_dir_elems
				(t_ls_list **list, const char *path, const char*prg_n);
void					ft_ls_print
				(const t_ls_args *ls_args, const t_ls_list *element);
void					ft_ls_compute
			(t_ls_list *restrict list_head, const t_ls_args *restrict ls_args);
void					ft_list_clear_all(t_ls_list **head);
void					ft_work
			(t_ls_args *ls_args, struct s_ls_files *ls_files);
void					ft_print_error(const char *prg_n, const char *path);
void					ft_help(void);
void					ft_ls
		(const t_ls_args *restrict ls_args, t_ls_list **restrict list_head);
void					ft_ls_print_default(const t_ls_args *restrict ls_args,
							const t_ls_list *restrict the_element);
void					ft_ls_print_long(const t_ls_args *restrict ls_args,
							const t_ls_list *restrict the_element);

#endif

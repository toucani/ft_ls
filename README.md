# ft_ls

ft_ls is my own ls implementation which supports *1afGlRrStu* flags and *--help*.


Flags meaning:

* 1 - output in one column
* a - show files starting with '.'
*	f - not sorting the output
*	G - colorful output
*	l - long format of output
*	R - recursive
*	r - reverse sorting order
*	S - sort by file size
*	t - sort by last modification time
*	u - sort by last access time (only with 't')


Currently it works only on Mac and BSD(maybe, no one tested it on BSD).

## Installing

Clone this repository recursively
```
git clone --recurse-submodules
```
and
```
make
```

## License
![gplv3-88x31.png](https://www.gnu.org/graphics/gplv3-88x31.png)

This project is licensed under the GNU GPL License, Version 3 - see [LICENSE.md](LICENSE.md) for details.

## Contributing/Issue creating

Raise an issue in the issue tracker, or write me a letter.

## Contacts

* [Bitbucket](https://bitbucket.org/Mitriksicilian/)
* [E-mail](mailto:MitrikSicilian@icloud.com?subject=ft_ls from Bitbucket)
* MitrikSicilian@icloud.com

## Many thanks

* to UNIT Factory, for inspiration to do my best.
* to all UNIT Factory students, who shared their knowledge with me and tested this project.

## UNIT Factory
![UNIT Factory logo](https://unit.ua/static/img/logo.png)

[UNIT Factory](https://uk.wikipedia.org/wiki/UNIT_Factory) was an innovative programming school and a part of [School 42](https://en.wikipedia.org/wiki/42_(school)) in [the heart](https://en.wikipedia.org/wiki/Kyiv) of [Ukraine](https://en.wikipedia.org/wiki/Ukraine).
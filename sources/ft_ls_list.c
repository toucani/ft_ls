/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 16:30:08 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/12 16:30:09 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"
#include <sys/stat.h>

t_ls_list	*ft_list_create_elem(const char *name, const char *full_path)
{
	t_ls_list	*new;

	new = ft_memalloc(sizeof(t_ls_list));
	new->print.name = ft_strdup(name);
	new->full_path = ft_strdup(full_path);
	lstat(full_path ? full_path : name, &(new->stats));
	return (new);
}

void		ft_list_push_back(t_ls_list **the_list, t_ls_list *restrict elem,
		const t_ls_args *restrict ls_args)
{
	t_ls_list	*temp;

	if (!(*the_list))
		*the_list = elem;
	else
	{
		if ((ls_args->sorting(*the_list, elem) * ls_args->reverse) < 0)
		{
			elem->next = *the_list;
			*the_list = elem;
			return ;
		}
		temp = *the_list;
		while (temp->next)
			if ((ls_args->sorting(temp->next, elem) * ls_args->reverse) < 0)
			{
				elem->next = temp->next;
				temp->next = elem;
				return ;
			}
			else
				temp = temp->next;
		temp->next = elem;
	}
}

void		ft_list_clear_all(t_ls_list **head)
{
	t_ls_list	*temp;
	t_ls_list	*temp2;

	temp = *head;
	*head = 0;
	while (temp)
	{
		ft_strdel(&(temp->print.name));
		ft_strdel(&(temp->full_path));
		temp2 = temp->next;
		ft_memdel((void**)&temp);
		temp = temp2;
	}
}

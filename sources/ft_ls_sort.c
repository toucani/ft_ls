/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 15:56:22 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/21 15:56:23 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"

int	ft_sort_default(const t_ls_list *restrict param1,
				const t_ls_list *restrict param2)
{
	if (!param1 || !param2)
		return (0);
	return (ft_strcmp(param2->print.name, param1->print.name));
}

int	ft_sort_time_access(const t_ls_list *restrict param1,
				const t_ls_list *restrict param2)
{
	int	rt;

	if (!param1 || !param2)
		return (0);
	rt = param1->stats.st_atimespec.tv_sec - param2->stats.st_atimespec.tv_sec;
	if (!rt)
		rt = param1->stats.st_atimespec.tv_nsec -
				param2->stats.st_atimespec.tv_nsec;
	return ((rt) ? rt : ft_sort_default(param1, param2));
}

int	ft_sort_time_modify(const t_ls_list *restrict param1,
				const t_ls_list *restrict param2)
{
	int	rt;

	if (!param1 || !param2)
		return (0);
	rt = param1->stats.st_mtimespec.tv_sec - param2->stats.st_mtimespec.tv_sec;
	if (!rt)
		rt = param1->stats.st_mtimespec.tv_nsec -
				param2->stats.st_mtimespec.tv_nsec;
	return ((rt) ? rt : ft_sort_default(param1, param2));
}

int	ft_sort_size(const t_ls_list *restrict param1,
				const t_ls_list *restrict param2)
{
	int	rt;

	if (!param1 || !param2)
		return (0);
	rt = param1->stats.st_size - param2->stats.st_size;
	return ((rt) ? rt : ft_sort_default(param1, param2));
}

int	ft_sort_no(const t_ls_list *restrict param1,
				const t_ls_list *restrict param2)
{
	if (!param1 || !param2)
		return (0);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_print.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:46:14 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/13 17:46:15 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"
#include <time.h>
#include <pwd.h>
#include <grp.h>
#include <sys/stat.h>
#include "ft_printf.h"
#include <sys/ioctl.h>
#include <sys/xattr.h>

/*
**	Default printing function.
**	Prints either in one column or in multiple columns.
**	printed_width = ioctl just to make it zero
*/

void		ft_ls_print_default(const t_ls_args *restrict ls_args,
			const t_ls_list *restrict the_element)
{
	struct winsize	window;
	size_t			printed_width;

	printed_width = ioctl(STDOUT_FILENO, TIOCGWINSZ, &window);
	while (the_element)
	{
		if (the_element->print.name[0] != '.' ||
		ft_strchr(ls_args->args, ARG_SHOW_DOT) ||
			ft_strchr(ls_args->args, ARG_SORT_NO))
		{
			if (ft_strchr(ls_args->args, ARG_ONE_COL))
				ft_printf("%r%s%r\n", the_element->print.name_color,
					the_element->print.name, ls_args->color_def);
			else
			{
				ft_putstr((printed_width + the_element->print.name_width
					>= window.ws_col && !(printed_width = 0)) ? "\n" : "");
				printed_width += ft_printf("%r%-*s",
				the_element->print.name_color, the_element->print.name_width,
				the_element->print.name);
			}
		}
		the_element = the_element->next;
	}
	ft_printf("%r%s", ls_args->color_def, (printed_width) ? "\n" : "");
}

/*
**	Long format functions.
**	Counts and prints the total message.
*/

static void	ft_ls_print_long_total(const t_ls_args *restrict ls_args,
				t_ls_list *restrict the_element)
{
	size_t	ct;

	ct = 0;
	while (the_element)
	{
		if (the_element->print.name[0] != '.' ||
			ft_strchr(ls_args->args, ARG_SHOW_DOT) ||
			ft_strchr(ls_args->args, ARG_SORT_NO))
			ct += the_element->stats.st_blocks;
		the_element = the_element->next;
	}
	ft_printf("total %d\n", ct);
}

/*
**	This functions prints d,rwx(s),rwx(s),rwx(t) and @
*/

static void	ft_ls_print_long_rwx(const t_ls_list *restrict elem, char *temp)
{
	*temp = S_ISBLK(elem->stats.st_mode) ? 'b' : '-';
	*temp = S_ISCHR(elem->stats.st_mode) ? 'c' : *temp;
	*temp = S_ISDIR(elem->stats.st_mode) ? 'd' : *temp;
	*temp = S_ISLNK(elem->stats.st_mode) ? 'l' : *temp;
	*temp = S_ISFIFO(elem->stats.st_mode) ? 'p' : *temp;
	*temp = S_ISSOCK(elem->stats.st_mode) ? 's' : *temp;
	ft_putchar(*temp);
	*temp = (S_ISXUSR(elem->stats.st_mode)) ? 'x' : '-';
	if (S_ISSUID(elem->stats.st_mode))
		*temp = (*temp == '-') ? 'S' : 's';
	ft_printf("%c%c%c", S_ISRUSR(elem->stats.st_mode) ? 'r' : '-',
	S_ISWUSR(elem->stats.st_mode) ? 'w' : '-', *temp);
	*temp = (S_ISXGRP(elem->stats.st_mode)) ? 'x' : '-';
	if (S_ISSGID(elem->stats.st_mode))
		*temp = (*temp == '-') ? 'S' : 's';
	ft_printf("%c%c%c", S_ISRGRP(elem->stats.st_mode) ? 'r' : '-',
	S_ISWGRP(elem->stats.st_mode) ? 'w' : '-', *temp);
	*temp = (S_ISXOTH(elem->stats.st_mode)) ? 'x' : '-';
	if (S_ISSTCK(elem->stats.st_mode))
		*temp = (*temp == '-') ? 'T' : 't';
	ft_printf("%c%c%c", S_ISROTH(elem->stats.st_mode) ? 'r' : '-',
	S_ISWOTH(elem->stats.st_mode) ? 'w' : '-', *temp);
	ft_putchar(
		(listxattr(elem->full_path, 0, 0, XATTR_NOFOLLOW) > 0) ? '@' : ' ');
}

/*
**	Prints number of links, user name, group name, size of the file and time.
*/

static void	ft_ls_print_long_ln_names_size_time(
				const t_ls_list *restrict the_element)
{
	char		*temp_str;
	time_t		seconds_now;
	struct stat	*curr_stats;

	curr_stats = &(((t_ls_list*)the_element)->stats);
	temp_str = ft_uitoa(curr_stats->st_nlink);
	ft_printf("%*s %-*s %-*s", the_element->print.field_width[0], temp_str,
	the_element->print.field_width[1], getpwuid(curr_stats->st_uid)->pw_name,
	the_element->print.field_width[2], getgrgid(curr_stats->st_gid)->gr_name);
	ft_strdel(&temp_str);
	temp_str = ft_uitoa(curr_stats->st_size);
	ft_printf("%*s ", the_element->print.field_width[3], temp_str);
	ft_strdel(&temp_str);
	seconds_now = time(0);
	temp_str = ft_strnew(20);
	ft_strncpy(temp_str, &(ctime(&(curr_stats->st_mtimespec.tv_sec))[4]), 20);
	if (seconds_now - curr_stats->st_mtimespec.tv_sec > SECS_IN_HALF_YEAR)
	{
		temp_str[7] = ' ';
		ft_strncpy(&(temp_str[8]), &(temp_str[16]), 5);
	}
	ft_printf("%-12.12s ", temp_str);
	ft_strdel(&temp_str);
}

void		ft_ls_print_long(const t_ls_args *restrict ls_args,
				const t_ls_list *restrict the_element)
{
	char *temp_str;

	temp_str = ft_strnew(MAX_FILENAME_LENGTH);
	ft_ls_print_long_total(ls_args, (t_ls_list *)the_element);
	while (the_element)
	{
		if (the_element->print.name[0] != '.' ||
			ft_strchr(ls_args->args, ARG_SHOW_DOT) ||
			ft_strchr(ls_args->args, ARG_SORT_NO))
		{
			ft_ls_print_long_rwx(the_element, temp_str);
			ft_ls_print_long_ln_names_size_time(the_element);
			ft_printf("%r%s%r", the_element->print.name_color,
				the_element->print.name, "reset");
			if (S_ISLNK(the_element->stats.st_mode))
			{
				readlink(the_element->full_path, temp_str, MAX_FILENAME_LENGTH);
				ft_printf(" -> %s", temp_str);
				ft_bzero(temp_str, MAX_FILENAME_LENGTH);
			}
			ft_putchar('\n');
		}
		the_element = the_element->next;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/12 16:30:00 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/12 16:30:01 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"
#include "ft_printf.h"
#include <dirent.h>
#include <stdio.h>
#include <errno.h>

/*
**	This function tries to open every path in <check.paths>.
**	If it is possible	->	adds it to the dirs.paths
**	if it is not a dir	->	adds it to the files.paths
**		else			->	prints error message.
**
**	This functions changes ERRNO!
*/

static void			ft_check_files(struct s_ls_files *ls_files,
								const char *prg_name)
{
	unsigned int	ct;
	DIR				*test_dir;

	ct = 0;
	ls_files->dirs.count = 0;
	ls_files->files.count = 0;
	ls_files->dirs.paths =
		(char**)ft_memalloc(sizeof(char*) * (ls_files->check.count + 1));
	ls_files->files.paths =
		(char**)ft_memalloc(sizeof(char*) * (ls_files->check.count + 1));
	while (ls_files->check.paths[ct] && ct < ls_files->check.count)
	{
		test_dir = opendir(ls_files->check.paths[ct++]);
		if (test_dir && !closedir(test_dir))
			ls_files->dirs.paths[ls_files->dirs.count++] =
				ls_files->check.paths[ct - 1];
		else if (errno == ENOTDIR)
			ls_files->files.paths[ls_files->files.count++] =
				ls_files->check.paths[ct - 1];
		else
			ft_print_error(prg_name, ls_files->check.paths[ct - 1]);
	}
	if (!ls_files->check.paths[0])
		ls_files->dirs.paths[ls_files->dirs.count++] = ".";
	errno = 0;
}

/*
**	Checks arguments,
**	if some of them is unknown	->	prints <usage> message and exits.
*/

static void			ft_check_args(const t_ls_args *ls_args)
{
	size_t		ct;

	ct = 0;
	while (ls_args->args && ls_args->args[ct])
		if (!ft_strchr(ALL_ARGS, ls_args->args[ct]))
		{
			ft_printf("%s: illegal option -- %c\n",
				&(ls_args->program_name[2]), ls_args->args[ct]);
			ft_printf("usage: %s [-"ALL_ARGS"] [file...]\n",
				&(ls_args->program_name[2]));
			exit(1);
		}
		else
			ct++;
}

/*
**	Setting default colors
*/

static void			ft_ls_set_colors(t_ls_args *restrict ls_args)
{
	ls_args->color_fld = (ft_strchr(ls_args->args, ARG_COLOR)) ? COLOR_FLD : 0;
	ls_args->color_exe = (ft_strchr(ls_args->args, ARG_COLOR)) ? COLOR_EXE : 0;
	ls_args->color_lnk = (ft_strchr(ls_args->args, ARG_COLOR)) ? COLOR_LNK : 0;
	ls_args->color_fifo = (ft_strchr(ls_args->args, ARG_COLOR)) ? COLOR_FIF : 0;
	ls_args->color_def = (ft_strchr(ls_args->args, ARG_COLOR)) ? COLOR_DEF : 0;
}

/*
**	This functions parses argv and argc to get all the neccesary args,
**	and adds everything after that to paths array.
**	It also assigns colors, if 'G' is present in the args.
**
**	reverse value must be -1 or 1, it mustn't be 0,
**		or sorting will never happen!
*/

static void			ft_get_params(const int ac, const char **av,
							t_ls_args *ls_args, struct s_ls_files *ls_files)
{
	int32_t			ct;

	ct = 1;
	ls_args->args = 0;
	ls_args->program_name = av[0];
	while (ct < ac && av[ct][0] == '-' && av[ct][1])
		ls_args->args =
			ft_strjoin_del_first((char**)&(ls_args->args), &av[ct++][1]);
	ft_ls_set_colors(ls_args);
	ls_args->reverse = (ft_strchr(ls_args->args, ARG_SORT_REV)) ? -1 : 1;
	ls_args->sorting = ft_sort_default;
	if (ft_strchr(ls_args->args, ARG_SORT_SIZE))
		ls_args->sorting = ft_sort_size;
	else if (ft_strchr(ls_args->args, ARG_SORT_NO))
		ls_args->sorting = ft_sort_no;
	else if (ft_strchr(ls_args->args, ARG_SORT_T_MOD) &&
		ft_strchr(ls_args->args, ARG_SORT_T_ACC))
		ls_args->sorting = ft_sort_time_access;
	else if (ft_strchr(ls_args->args, ARG_SORT_T_MOD))
		ls_args->sorting = ft_sort_time_modify;
	ls_files->check.count = ac - ct;
	ls_files->check.paths = (char **)&(av[ct]);
}

/*
**	Main function. It forms the argument strcuture and checks is all the args
**	are ok. After that it checks every directory path, whether it can be opened
**	and forms directory and files arrays.
*/

int					main(const int ac, const char **av)
{
	t_ls_args			ls_args;
	struct s_ls_files	ls_files;
	int					ct;

	ct = 0;
	while (++ct < ac)
		if (av[ct] && ft_strstr(av[ct], "help"))
			ft_help();
	ft_get_params(ac, av, &ls_args, &ls_files);
	ft_check_args(&ls_args);
	ft_check_files(&ls_files, ls_args.program_name);
	ft_work(&ls_args, &ls_files);
	exit(0);
}

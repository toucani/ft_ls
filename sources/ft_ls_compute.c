/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_compute.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 12:33:26 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/27 12:33:28 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"
#include <pwd.h>
#include <grp.h>

/*
**	Computing the minimum width for printf!
**
**	It gets the widths of 4 fields:
**		field_max[0]	-	width of the links	section
**		field_max[1]	-	width of the uid	section
**		field_max[2]	-	width of the gid	section
**		field_max[3]	-	width of the size	section
**	and stores them into the list_head.
*/

static void	ft_ls_compute_get_values(t_ls_list *list_head)
{
	uint16_t		*name_max;
	uint16_t		*fld_max;
	char			*temp_str[4];
	uint16_t		ct;

	if (!list_head)
		return ;
	name_max = &(list_head->print.name_width);
	fld_max = &(list_head->print.field_width[0]);
	while (list_head)
	{
		if (ft_strlen(list_head->print.name) > *name_max)
			*name_max = ft_strlen(list_head->print.name);
		temp_str[0] = ft_uitoa(list_head->stats.st_nlink);
		temp_str[1] = getpwuid(list_head->stats.st_uid)->pw_name;
		temp_str[2] = getgrgid(list_head->stats.st_gid)->gr_name;
		temp_str[3] = ft_uitoa(list_head->stats.st_size);
		ct = 0;
		while (ct++ < 4)
			if (ft_strlen(temp_str[ct - 1]) > fld_max[ct - 1])
				fld_max[ct - 1] = ft_strlen(temp_str[ct - 1]);
		ft_strdel(&temp_str[0]);
		ft_strdel(&temp_str[3]);
		list_head = list_head->next;
	}
}

/*
**	Just copying the field_width and name_width from the headof the list to
**	all othe elements.
*/

static void	ft_ls_compute_set_values(t_ls_list *list_head)
{
	t_ls_list		*temp;
	uint16_t		*name_max;
	uint16_t		*field_max;

	temp = list_head;
	name_max = &(list_head->print.name_width);
	field_max = &(list_head->print.field_width[0]);
	(*name_max)++;
	field_max[1]++;
	field_max[2]++;
	field_max[3]++;
	while (temp)
	{
		temp->print.name_width = *name_max;
		ft_memcpy(&(temp->print.field_width), field_max,
			sizeof(temp->print.field_width));
		temp = temp->next;
	}
}

static void	ft_ls_compute_set_colors(t_ls_list *list_element,
								const t_ls_args *restrict ls_args)
{
	while (list_element)
	{
		list_element->print.name_color = (char*)ls_args->color_def;
		if (S_ISDIR(list_element->stats.st_mode))
			list_element->print.name_color = (char*)ls_args->color_fld;
		else if (S_ISLNK(list_element->stats.st_mode))
			list_element->print.name_color = (char*)ls_args->color_lnk;
		else if (S_ISFIFO(list_element->stats.st_mode))
			list_element->print.name_color = (char*)ls_args->color_fifo;
		else if (S_ISEXE(list_element->stats.st_mode))
			list_element->print.name_color = (char*)ls_args->color_exe;
		list_element = list_element->next;
	}
}

/*
**	Computing the minimum width for printf and colors!
**	If ls-args->color_fld(or exe) then we have G in args, so we dint
**	need to check it again
*/

void		ft_ls_compute(t_ls_list *restrict list_head,
						const t_ls_args *restrict ls_args)
{
	ft_ls_compute_get_values(list_head);
	ft_ls_compute_set_values(list_head);
	if (ft_strchr(ls_args->args, ARG_COLOR))
		ft_ls_compute_set_colors(list_head, ls_args);
}

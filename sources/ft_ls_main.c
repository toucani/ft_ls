/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_main.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 15:56:22 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/21 15:56:23 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"
#include "ft_printf.h"
#include <stdio.h>
#include <errno.h>
#include <dirent.h>

/*
**	Function that works with directories.
**	It opens a directory (if it can), and pushes all contents into the list.
*/

static void		ft_ls_dirs(const t_ls_args *restrict ls_args,
									const char *restrict path)
{
	DIR				*directory;
	t_ls_list		*list_head;
	struct dirent	*element;
	char			*tmp;

	list_head = 0;
	if (!(directory = opendir(path)))
	{
		tmp = ft_strrchr(path, '/');
		ft_print_error(ls_args->program_name, tmp ? tmp + 1 : path);
		return ;
	}
	while ((element = readdir(directory)))
	{
		tmp = ft_strjoin_f(path, "/", element->d_name, "");
		ft_list_push_back(&list_head,
			ft_list_create_elem(element->d_name, tmp), ls_args);
		ft_strdel(&tmp);
	}
	closedir(directory);
	ft_ls(ls_args, &list_head);
	ft_list_clear_all(&list_head);
}

/*
**	Function whic works with files, which were given as arguments.
*/

static void		fs_ls_files(const t_ls_args *restrict ls_args,
							const struct s_paths *files)
{
	t_ls_list		*list_head;
	unsigned int	ct;

	if (!files->count || !files->paths[0])
		return ;
	ct = 0;
	list_head = 0;
	while (files->paths[ct] && ct < files->count)
		ft_list_push_back(&list_head,
			ft_list_create_elem(files->paths[ct++], 0), ls_args);
	ft_ls(ls_args, &list_head);
	ft_list_clear_all(&list_head);
}

/*
**	The LS! :)
**	Get args and the list of elements, whic the function sort,
**	computes the maxim length and prints. If neccesary -> starts a recursion.
**	It can't be static as it needs ft_ls_dirs, and ft_ls_dirs needs ft_ls...
*/

void			ft_ls(const t_ls_args *restrict ls_args,
						t_ls_list **restrict list_head)
{
	t_ls_list	*the_element;

	ft_ls_compute(*list_head, ls_args);
	if (ft_strchr(ls_args->args, ARG_LONG))
		ft_ls_print_long(ls_args, *list_head);
	else
		ft_ls_print_default(ls_args, *list_head);
	if (ft_strchr(ls_args->args, ARG_RECURSIVE))
	{
		the_element = *list_head;
		while (the_element)
		{
			if (S_ISDIR(the_element->stats.st_mode) &&
				(ft_strchr(ls_args->args, ARG_SHOW_DOT) ||
					the_element->print.name[0] != '.') &&
				!ft_strequ(the_element->print.name, ".") &&
				!ft_strequ(the_element->print.name, ".."))
			{
				ft_printf("\n%s:\n", the_element->full_path);
				ft_ls_dirs(ls_args, the_element->full_path);
			}
			the_element = the_element->next;
		}
	}
}

/*
**	Creates s tring with error message and prints it.
**	This functions chages ERRNO!
*/

void			ft_print_error(const char *prg_n, const char *path)
{
	char *err_str;

	err_str = ft_strjoin_f(&prg_n[2], ": ", path, "");
	perror(err_str);
	ft_strdel(&err_str);
	errno = 0;
}

/*
**	Main working function
**	Just calls ls for all files, then for all dirs.
**	Prints dir name if neccesary.
*/

void			ft_work(t_ls_args *ls_args, struct s_ls_files *ls_files)
{
	unsigned int	ct;

	fs_ls_files(ls_args, &(ls_files->files));
	if (ls_files->files.count && ls_files->dirs.count)
		ft_putchar('\n');
	ct = 0;
	while (ls_files->dirs.paths[ct] && ct < ls_files->dirs.count)
	{
		if (ls_files->dirs.count > 1 || ls_files->files.count)
			ft_printf("%s%s:\n", (ct) ? "\n" : "", ls_files->dirs.paths[ct]);
		ft_ls_dirs(ls_args, ls_files->dirs.paths[ct++]);
	}
}

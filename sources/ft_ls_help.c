/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_help.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dkovalch <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/26 16:42:48 by dkovalch          #+#    #+#             */
/*   Updated: 2017/02/26 16:42:50 by dkovalch         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**   This file is part of ft_ls project.
**   Copyright (C) 2017 Dmytro Kovalchuk (mitriksicilian@icloud.com).
**
**   This program is free software: you can redistribute it and/or modify
**   it under the terms of the GNU General Public License as published by
**   the Free Software Foundation, version 3 of the License.
**
**   This program is distributed in the hope that it will be useful,
**   but WITHOUT ANY WARRANTY; without even the implied warranty of
**   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
**   GNU General Public License for more details.
**
**   You should have received a copy of the GNU General Public License
**   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ft_ls.h"
#include "ft_printf.h"

static void		man_name_usage_descriptions(void)
{
	ft_printf("\n%r%s%r\n\t", "bold", "NAME", "reset");
	ft_printf("%s\n", "ft_ls -- list directory contents.");
	ft_printf("\n%r%s%r\n\t", "bold", "USAGE", "reset");
	ft_printf("%s\n", "ft_ls [-"ALL_ARGS"] [file ...]");
	ft_printf("\n%r%s%r\n\t", "bold", "DESCRIPTION", "reset");
	ft_printf("%s%r%s%r%s\n\t%s\n\t",
	"For each operand that names a ", "_", "file", "reset",
	" of a type other than directory, ft_ls displays",
	"its name as well as any requested, associated information.");
	ft_printf("%s%r%s%r%s\n\t%s\n\t%s\n\n",
	"For each operand that names a ", "_", "file", "reset",
	" of type directory, ft_ls displays the names",
	"of files contained within that directory,"
	" as well as any requested,", "associated information.");
	ft_printf("\t%s\n", "If no operands are given, the contents of the current"
		" directory are displayed.");
	ft_printf("\t%s\n", "If more than one operand is given, non-directory"
		"operands are displayed first;");
	ft_printf("\t%s\n", "directory and non-directory operands are sorted "
		"separately and in");
	ft_printf("\t%s\n", "lexicographical order.");
	ft_printf("\t%s\n", "It is free software distributed under GNU GPL license 3.");
}

static void		man_options_long(void)
{
	ft_printf("\n%r%s%r\n\t", "bold", "OPTIONS", "reset");
	ft_printf("\n\t%s\n", "-1\t(The numeric digit \'one\')"
		" Force output to be one entry per line.");
	ft_printf("\n\t%s\n", "-a\tInclude directory entries whose names begin with"
		" a dot (.).");
	ft_printf("\n\t%s\n", "-f\tOutput is not sorted. This option turns on "
	"the -a option.");
	ft_printf("\n\t%s\n", "-G\tEnable colorized output.");
	ft_printf("\n\t%s\n", "-l\t(The lowercase letter \'ell\') List in long"
	" format (see below).");
	ft_printf("\n\t%s\n", "-R\tRecursively list subdirectories encountered.");
	ft_printf("\n\t%s\n", "-r\tReverse the order of the sort, whatever sort is"
	" being used.");
	ft_printf("\n\t%s\n", "-S\tSort files by size.");
	ft_printf("\n\t%s\n", "-t\tSort by time modified (most recently modified"
	" first) before sorting\n\t\tthe operands by lexicographical order.");
	ft_printf("\n\t%s\n", "-u\tUse time of last access, instead of last "
	"modification of the file for\n\t\tsorting (-t) or long printing (-l).");
	ft_printf("\n%r%s%r\n\t", "bold", "LONG FORMAT (-l)", "reset");
	ft_printf("%s\n", "See original ls manual to learn about the long "
		"output format.");
}

static void		man_colors(void)
{
	ft_printf("\n%r%s%r\n\t", "bold", "COLORS", "reset");
	ft_printf("\n\t%r%s%r\t- %s\n", COLOR_DEF, "Default color", COLOR_DEF,
		"default color for files.");
	ft_printf("\n\t%r%s%r\t- %s\n", COLOR_FLD, COLOR_FLD, COLOR_DEF,
		"directories color.");
	ft_printf("\n\t%r%s%r\t- %s\n", COLOR_EXE, COLOR_EXE, COLOR_DEF,
		"executables color.");
	ft_printf("\n\t%r%s%r\t- %s\n", COLOR_LNK, COLOR_LNK, COLOR_DEF,
		"sym. links color.");
	ft_printf("\n\t%r%s%r\t- %s\n", COLOR_FIF, COLOR_FIF, COLOR_DEF,
		"socket and fifo color.");
}

/*
**	Prints man - if finds "help" in one of arguments.
**	look for the man in ft_ls_man.h
*/

void			ft_help(void)
{
	ft_printf("\n%rFT_LS\t\t\t\t\tGeneral Manual\t\t\t\t\tFT_LS%r\n\n",
		"bold", "reset");
	man_name_usage_descriptions();
	man_options_long();
	man_colors();
	ft_printf("\n%r%s%r\n\t", "bold", "STANDARTS", "reset");
	ft_printf("%s\n", "The ft_ls utility conforms to no standarts, except"
		" the author's standarts of");
	ft_printf("\t%s\n", "nice code and, inheritably, standarts original"
		"ls conforms to");
	ft_printf("\t%s\n", "(IEEE Std 1003.1-2001 (\'POSIX.1\')).");
	ft_printf("\n%r%s%r\n\t", "bold", "BUGS", "reset");
	ft_printf("%s\n", "No known bugs or pokemons are present.");
	ft_printf("\n%rLSD\t\t\t\t\tFebr 26, 2017\t\t\t\t\tLSD%r\n\n",
		"bold", "reset");
	exit(0);
}
